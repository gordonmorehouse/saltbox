.. Saltbox documentation master file, created by
   sphinx-quickstart on Fri Aug 10 00:41:02 2012.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Saltbox's documentation!
===================================

Contents:

.. toctree::
   :maxdepth: 2



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`


Talking to myself
=================

Architecture
------------

Saltbox has to live in multiple paradigms.  Since it is such a simple app, we
try to satisfy them all.  The best way, suggested by `George Dorn
<https://github.com/georgedorn>`_, was to (sort of) imitate MySQL, and divide
it (conceptually anyway) a standalone server and client.

In the case of Saltbox, the server exists on its own* and speaks three
different ways: XML-RPC, JSON via a RESTish interface, or direct API
calls if you choose to import it into your Python code.

The client exists on its own either as a command line app which speaks JSON via
stdin/stdout, or as small libraries for a large variety of languages (won't you
contribute?) which speak XML-RPC or JSON to the server over any method you may
choose to wire them up to the server.

The point of this architecture is *ease of integration.*  We want everyone to
have secure passwords.  We want the flood of "password breach" and "wtf were
they doing storing passwords in cleartext/crypt/unsalted MD5" stories hitting
the news in mid-2012 to end.  So, we want it to be *very easy* for developers
- be they Microsoft ecosystem, LAMP ecosystem, Python ecosystem - to drop
Saltbox in and just go.  Hence providing both XML-RPC and JSON-based APIs.

FULL INTEGRATION
  Example: using Saltbox as a drop-in replacement for password storage in a
  Python app.  In this case, the Saltbox code lives right next to the rest of
  the web app.  If it's used in the Django framework, Saltbox can even write
  to the app's database using the Django ORM.

LOCAL SERVICE
  Example: using Saltbox as a drop-in replacement for shadow passwords on a
  Linux machine.  In this case, PAM calls the Saltbox command line program to
  create, verify, update or delete passwords.
  
  Example: using Saltbox to replace password storage on an insecure PHP web
  forum.  Saltbox runs as a network service on the loopback interface.

NETWORK SERVICE
  Example: using Saltbox as a backend for a company's entire stable of web
  apps written in several languages.  Saltbox runs as a network service on
  the intranet, and apps talk to it using the libraries provided for the
  language.  If there is no library, it's easy to write one because Saltbox
  uses standard interfaces: XML-RPC or JSON via REST/HTTP.

merh?
-----

Use saltbox.api.build_saltbox_server() to combine a class derived from SaltboxFrontend
and a class derived from SaltboxBackend.  It returns a SaltboxServer object.  In most
cases (@todo all???), one interacts with the SaltboxServer via its SaltboxFrontend.

SaltboxFrontend implements CRUD that the user (calling program, generally)
will use to interact with Saltbox.  It is the "buttons and knobs" portion of a
SaltboxServer instance.

Methods:

* create
* verify
* update
* delete

SaltboxBackend presents a very strict API for *any* SaltboxFrontend to talk to a
storage engine.

Methods:

* _db_read
* _db_write
* _db_update
* _db_delete



Why the complexity and multiple inheritance?
============================================

By creating a SaltboxServer from the composition of an interface (with which
programmers interact) and a storage backend (which handles storage for the
interface), we greatly increase the flexibility of Saltbox to be used with
any given app.

The API of the SaltboxFrontend and SaltboxBackend are very strictly defined.
You will find that supplie SaltboxBackend classes do a *lot* of sanity checking.
In this way, we ensure that any SaltboxFrontend can speak to any
SaltboxBackend with no fuss.
