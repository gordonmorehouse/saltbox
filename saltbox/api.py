# -*- coding: utf-8 -*-
"""
    Copyright 2012 Gordon Morehouse <gordon@morehouse.me>

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.

    API in brief
    ============

    key == unique primary key for the record; anything that can be made a string.
    data, new_data == string to be salted, hashed and then operated upon (created, verified, updated, deleted).
    owner == string identifying to which entity (program, service) the key-data pair belongs.

    create(key, data, owner) creates record and returns digest.

    verify(key, data, owner) verifies record and returns True (verified) or False.

    update(key, new_data, owner) updates record and returns digest.

    verify_then_update(key, data, new_data, owner) calls verify(); if okay, then calls update().

    delete(key, owner) deletes record.

    verify_then_delete(key, data, owner) calls verify(); if okay, then calls delete().


    Creating a SaltboxServer
    ========================

    class MySaltboxServer(MySaltboxInterface, MySaltboxBackend):
        pass

    sb = MySaltboxServer()

    Now interact with sb with the methods above:  sb.create(), sb.verify(), etc.
"""

from passlib.context import CryptContext


class SaltboxAdapter(object):
    """
    An API definition for interacting with SaltboxFrontends.  (Akin to a Java
    Interface, if that makes more sense for Python noobs who scratch their
    heads at the 'raise NotImplementedError' in every method.)

    SaltboxAdapter objects used by the programmer will talk to SaltboxServer
    objects, which multiply inherit from a SaltboxFrontend (JSON, XML-RPC) and
    a SaltboxBackend (SQLite, etc)
    """

    def create(cls, key, data, owner="default"):
        """
        Create a saltbox record.

        @param (string) owner Represents the key:data pair's owner
        @param (string) key Used to look up the (hashed) data
        @param (string) data Will be hashed and stored for verification
        @return string The digest of data that was stored
        @raise SaltboxException
        """
        raise NotImplementedError


    def verify(cls, key, data, owner="default"):
        """
        Verify a saltbox record.

        @param (string) owner Represents the key:data pair's owner
        @param (string) key Used to look up the (hashed) data
        @param (string) data Will be hashed and checked against stored hash for given key
        @return boolean True if data is verified against existing record; False if not verified
        @raise KeyError if no record.
        @raise SaltboxException
        """
        raise NotImplementedError


    def update(cls, key, new_data, owner="default"):
        """
        Update a saltbox record with new data.

        @param (string) key Used to look up the (hashed) data
        @param (string) new_data Will be hashed and stored for verification, replacing existing hashed data
        @param (string) owner Represents the key:data pair's owner
        @return string The digest of data that was stored
        @raise SaltboxException
        """
        raise NotImplementedError


    def verify_then_update(cls, key, data, new_data, owner="default"):
        """
        Update a saltbox record with new data, first verifying that data
        verifies against the hash to be updated (i.e. "to change your
        password you must first enter your old password.")

        @param (string) key Used to look up the (hashed) data
        @param (string) data Will be used in verify() and update will fail if verify fails
        @param (string) new_data Will be hashed and stored for verification, replacing existing hashed data
        @param (string) owner Represents the key:data pair's owner
        @return mixed The string digest of data that was stored, or False if verification failed.
        @raise SaltboxException
        """
        if not self.verify(key, data, owner):
            return False

        return self.update(key, new_data, owner)


    def delete(cls, key, owner="default"):
        """
        Delete a saltbox record.

        @param (string) key Used to look up the (hashed) data
        @param (string) owner Represents the key:data pair's owner
        @raise SaltboxException
        """
        raise NotImplementedError


    def verify_then_delete(cls, key, data, owner="default"):
        """
        Delete a saltbox record, first verifying that data
        verifies against the hash to be updated (i.e. "to delete your
        account you must first enter your old password.")

        @param (string) key Used to look up the (hashed) data
        @param (string) data Will be used in verify() and delete will fail if verify fails
        @param (string) owner Represents the key:data pair's owner
        @return boolean True iff data verifies and deletion successful, False iff verification fails
        @raise SaltboxException
        """
        if not self.verify(key, data, owner):
            return False

        return self.delete(key, owner)


class SaltboxFrontend(object):
    def call(self, msg):
        raise NotImplementedError


class SaltboxBackend(object):
    """
    An interface for a SaltboxFrontend (the front end) to talk to some sort
    of a storage engine.  A strict API definition allows any SaltboxFrontend
    to use any SaltboxBackend.

    SaltboxServer objects used by the programmer will multiply inherit from a
    SaltboxFrontend and a SaltboxBackend.

    IMPORTANT: to get free sanity checking on arguments passed to your
    subclasses, call the equivalent SaltboxBackend method with super() from
    your subclass.  Thus you can avoid re-implementing boring sanity checks.

    This interface implements the following maximum string sizes.  See above
    for reasoning behind strict API definition.

    owner and key maximum 512 bytes to trade off between flexibility of
    people wishing to use arbitrary values for owner and key vs presumed
    cost of database lookup and disk storage.

    data maximum 8192 bytes because JSON is the first SaltboxFrontend and
    there's some evidence of vendor-specific JSON implementations having a
    default maximum JSON string size of 8KiB, and really, nobody will ever
    need a salted hash that takes up more than 8KiB... ;)
    """

    def __init__(self, cryptcontext=None):
        """
        Default constructor with sane defaults for cryptcontext.  Override
        if you need something different, otherwise this will probably be
        good.  If you override __init__ and don't deal with cryptcontext
        yourself, you need to call this superclass method!
        """
        if cryptcontext is None:
            # @todo make the CryptContext schemes not hard-coded
            cryptcontext = CryptContext(schemes=['ldap_pbkdf2_sha512'],
                                        default='ldap_pbkdf2_sha512',
                                        all__vary_rounds=0.2)

        self.cryptcontext = cryptcontext


    def _cc_digest(self, data):
        """
        Calculates the digest of data using self.cryptcontext.

        @param (string) data
        @return string The hashed, salted digest.
        """
        return self.cryptcontext.encrypt(data)


    def _cc_verify(self, data, digest):
        """
        Verifies that data hashes to digest using self.cryptcontext.

        @param (string) data
        @param (string) digest
        @return boolean True if data verifies vs digest, else False.
        """
        return self.cryptcontext.verify(data, self.digest_str)


    def _salt(self, owner, key, data):
        """
        Override this to perform your own salting in addition to that
        provided by self.cryptcontext.  For example, this may be useful
        for shared-secret type schemes. @todo (example)

        If not overridden, no permutation is done on data.

        @param (string) owner
        @param (string) key
        @param (string) data
        @return string data with some kind of permutation done to it.
        """
        return data


    def _db_read(self, owner, key):
        """
        @param (string) owner
        @param (string) key
        @return string The digest associated with (owner, key)
        @raise KeyError if record not found, or if owner/key are blank
        """
        if not owner or not key:
            raise KeyError, "owner and key must not be blank"

        if self.__class__.__name__ != 'SaltboxBackend':
            return

        raise NotImplementedError


    def _db_write(self, owner, key, data):
        """
        @param (string) owner
        @param (string) key
        @param (string) data
        @return string The digest created for data
        @raise KeyError if owner/key are blank; where owner/key longer than 512; if overwrite attempted
        @raise ValueError if data is None or is longer than 8192
        """
        if not owner or not key:
            raise KeyError, "owner and key must not be blank"
        if data is None:
            raise ValueError, "data must be string, not None"

        if len(owner) > 512:
            raise KeyError, "owner exceeds maximum length of 512"

        if len(key) > 512:
            raise KeyError, "key exceeds maximum length of 512"

        if len(data) > 8192:
            raise ValueError, "data exceeds maximum length of 8192"

        # this whole block handles only raising a KeyError if the record the
        # caller wants to write already exists, in which case they should
        # update, instead.  it smells bad.
        try:
            self._db_read(owner, key)   # not found = KeyError (which we want)
            e = KeyError("record exists, cannot write")
            e._sbb = True
            raise e
        except KeyError, e:
            if hasattr(e, '_sbb'):
                raise
            else:
                pass

        if self.__class__.__name__ != 'SaltboxBackend':
            return

        raise NotImplementedError


    def _db_update(self, owner, key, data):
        """
        @param (string) owner
        @param (string) key
        @param (string) data
        @return string The digest created for data
        @raise KeyError if record not found to update; where owner/key longer than 512; where _db_read raises KeyError
        @raise ValueError if data is None or is longer than 8192
        """
        if not owner or not key:
            raise KeyError, "owner and key must not be blank"
        if data is None:
            raise ValueError, "data must be string, not None"

        if len(owner) > 512:
            raise KeyError, "owner exceeds maximum length of 512"

        if len(key) > 512:
            raise KeyError, "key exceeds maximum length of 512"

        if len(data) > 8192:
            raise ValueError, "data exceeds maximum length of 8192"

        if self.__class__.__name__ != 'SaltboxBackend':
            return

        raise NotImplementedError


    def _db_delete(self, owner, key):
        """
        @param (string) owner
        @param (string) key
        @return void
        @raise KeyError if record not found to delete
        """
        if self.__class__.__name__ != 'SaltboxBackend':
            return

        raise NotImplementedError


#
# This factory function is probably getting 86'd.  Leaving it here for a bit
# in case I want it.
#
# See comments from the API doc at the top of the file; it's as simple for
# the Python programmer as:
#
#    class MySaltboxServer(MySaltboxInterface, MySaltboxBackend):
#        pass
#
#    sb = MySaltboxServer()
#
# and this plays better with IDEs.

#def build_saltbox_server(interface_klass, backend_klass):
    #"""
    #Factory function which hands back a SaltboxServer class composed of the
    #given SaltboxFrontend and SaltboxBackend.
    #"""
    #errors = []
    #if not isinstance(interface_klass, SaltboxFrontend):
        #errors[] = 'interface_klass must inherit from SaltboxFrontend'
    #if not isinstance(messenger_klass, SaltboxBackend):
        #errors[] = 'messenger_klass must be a SaltboxBackend'

    #if errors:
        #if len(errors) == 2:
            #error = "%s and %s" % (errors[0], errors[1])
        #else:
            #error = errors[0]
        #raise SaltboxException(error)

    #class Saltbox(interface_klass, messenger_klass):
        #pass

    #return Saltbox()
