# -*- coding: utf-8 -*-
"""
    Copyright 2012 Gordon Morehouse <gordon@morehouse.me>

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
"""

from saltbox import SaltboxException
from saltbox.api import SaltboxAdapter


class SaltboxJSONAdapter(SaltboxAdapter):
    pass


class SaltboxXMLRPCAdapter(SaltboxAdapter):
    pass


