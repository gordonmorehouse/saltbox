# -*- coding: utf-8 -*-
"""
    Copyright 2012 Gordon Morehouse <gordon@morehouse.me>

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
"""

from saltbox import SaltboxException
from saltbox.api import SaltboxFrontend


class SaltboxJSONFrontend(SaltboxFrontend):
    """
    { 'call' : string from create, update, verify, delete,
      'owner' : string,
      'key' : string,
      'data' : string (only for create, update, verify) }
    """
    def __init__(self, saltbox_backend=None):
        self.sbb = saltbox_backend


    def call(self, msg):
        import json

        calldict = json.loads(msg)
        calltarget = self.sbb.getattr(calldict['call'])
        del calldict['call']

        retval = calltarget(**calldict)
        retval = json.dumps({'digest' : retval})
        return retval


class SaltboxXMLRPCFrontend(SaltboxFrontend):
    pass