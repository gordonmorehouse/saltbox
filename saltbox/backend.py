# -*- coding: utf-8 -*-
"""
    Copyright 2012 Gordon Morehouse <gordon@morehouse.me>

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
"""

from saltbox import SaltboxException
from saltbox.api import SaltboxBackend


class DictBackend(SaltboxBackend):
    """
    For testing.
      { owner : { key : digest, key : digest },
        owner : { key : digest, key : digest } }
    """
    storage = None
    owner = None

    def __init__(self):
        self.storage = {}
        super(DictBackend, self).__init__()


    def _db_read(self, owner, key):
        # free sanity checking from SaltboxBackend superclass
        super(DictBackend, self)._db_read(owner, key)

        if self.storage.has_key(owner):
            if self.storage[owner].has_key(key):
                return self.storage[owner][key]

        raise KeyError, "record not found"


    def _db_write(self, owner, key, data):
        # free sanity checking from SaltboxBackend superclass
        super(DictBackend, self)._db_write(owner, key, data)

        digest = self._cc_digest(data)

        if not self.storage.has_key(owner):
            self.storage[owner] = {key : digest}
        else:
            self.storage[owner][key] = digest

        return digest


    def _db_update(self, owner, key, data):
        # free sanity checking from SaltboxBackend superclass
        super(DictBackend, self)._db_update(owner, key, data)

        self._db_read(owner, key)   # raises KeyError if no record to update
        digest = self._cc_digest(data)
        self.storage[owner][key] = digest
        return digest


    def _db_delete(self, owner, key):
        # free sanity checking from SaltboxBackend superclass
        super(DictBackend, self)._db_delete(owner, key)

        self._db_read(owner, key)   # raises KeyError if no record to delete
        del self.storage[owner][key]



class AlchemyBackend(SaltboxBackend):
    """
    Class for SQLAlchemy 'back end' (@todo ??? running on 'the same box'?)

    This should take a DSN and just use anything SQLAlchemy supports.
    """

    def __init__(self, engine='sqlite:///:memory:'):
        super(AlchemyBackend, self).__init__()

        from sqlalchemy import schema, types

        self.metadata = schema.MetaData()

        self.sb_tbl = schema.Table('saltbox', self.metadata,
            schema.Column('owner', types.String(512), primary_key=True),
            schema.Column('key', types.String(512), primary_key=True),
            schema.Column('digest', types.String(8192), default='')
        )

        from sqlalchemy.engine import create_engine

        engine = create_engine(engine, echo=False)
        self.metadata.bind = engine
        self.metadata.create_all(checkfirst=True)
        self.connection = engine.connect()


    def _db_read(self, owner, key):
        # free sanity checking from SaltboxBackend superclass
        super(AlchemyBackend, self)._db_read(owner, key)

        from sqlalchemy.sql import select, and_

        s = select([self.sb_tbl.c.digest],
                   and_(self.sb_tbl.c.key==key, self.sb_tbl.c.owner==owner))
        res = self.connection.execute(s)

        digest = res.scalar()
        if digest is None:
            raise KeyError, "record not found"
        else:
            return digest.encode('utf-8')


    def _db_write(self, owner, key, data):
        # free sanity checking from SaltboxBackend superclass
        super(AlchemyBackend, self)._db_write(owner, key, data)

        digest = self._cc_digest(data)

        values = {'owner' : owner, 'key' : key, 'digest' : digest}
        ins = self.sb_tbl.insert(values)
        res = self.connection.execute(ins)
        if res.inserted_primary_key:
            return res.last_inserted_params()['digest']


    def _db_update(self, owner, key, data):
        # free sanity checking from SaltboxBackend superclass
        super(AlchemyBackend, self)._db_update(owner, key, data)

        from sqlalchemy.sql import and_

        self._db_read(owner, key)   # raises KeyError if no record to update
        digest = self._cc_digest(data)

        upd = self.sb_tbl.update().where(and_(self.sb_tbl.c.key==key, self.sb_tbl.c.owner==owner)).values({'digest' : digest})
        res = self.connection.execute(upd)

        assert(res.rowcount == 1)
        return res.last_updated_params()['digest']


    def _db_delete(self, owner, key):
        # free sanity checking from SaltboxBackend superclass
        super(AlchemyBackend, self)._db_delete(owner, key)

        from sqlalchemy.sql import and_

        self._db_read(owner, key)   # raises KeyError if no record to delete
        dl = self.sb_tbl.delete().where(and_(self.sb_tbl.c.key==key, self.sb_tbl.c.owner==owner))
        res = self.connection.execute(dl)


#class SQLiteBackend(AlchemyBackend):
    #pass


#class MySQLBackend(AlchemyBackend):
    #pass


#class PostgreSQLBackend(AlchemyBackend):
    #pass


############## pipe dreams...

class POSIXPasswdBackend(SaltboxBackend):
    pass


class MemcacheBackend(SaltboxBackend):
    pass


class PAMBackend(SaltboxBackend):
    pass


class DotNetBullshitBackend(SaltboxBackend):
    pass
