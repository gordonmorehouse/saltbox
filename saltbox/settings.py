# -*- coding: utf-8 -*-
"""
    Copyright 2012 Gordon Morehouse <gordon@morehouse.me>

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
"""

import passlib.hash


# @todo should this go here?  or exist at all?
# CHANGE ME TO YOUR FAVORITE PARAGRAPH
static_salt = """
[When Vonnegut tells his wife he's going out to buy an envelope]

Oh, she says, well, you're not a poor man. You know, why don't you go online and
buy a hundred envelopes and put them in the closet? And so I pretend not to hear
her. And go out to get an envelope because I'm going to have a hell of a good
time in the process of buying one envelope. I meet a lot of people. And, see
some great looking babes. And a fire engine goes by. And I give them the thumbs
up. And, and ask a woman what kind of dog that is. And, and I don't know. The
moral of the story is, is we're here on Earth to fart around. And, of course,
the computers will do us out of that. And, what the computer people don't
realize, or they don't care, is we're dancing animals. You know, we love to move
around. And, we're not supposed to dance at all anymore.

Kurt Vonnegut, Jr.
Interview by David Brancaccio, NOW (PBS) (7 October 2005)
http://www.pbs.org/now/arts/vonnegut.html

Source: https://en.wikiquote.org/wiki/Kurt_Vonnegut (retrieved 2012-08-14)
Excerpt license: https://creativecommons.org/licenses/by-sa/3.0/
"""


# Recommended values include the default (hash.sha512_crypt, which yields a
# string about 110 bytes and is fast on 64-bit machines); hash.sha256_crypt,
# which yields a string about 65 bytes and is fast on 32-bit machines.
#
# Not recommended is hash.plaintext, which can be used if human readability
# is of the utmost importance.


# db_key derivation functions must produce no more than 512 bytes of output.
# sha512/sha256 provide good basic security against an attacker being able
# to easily determine the owner and key of a given record upon stealing the
# database, while still allowing quick lookup in many storage engines.
db_key_schemes = ['sha512_crypt']    # @todo upgrading

db_owner_derivation_function = hash.sha512_crypt
db_key_derivation_function = hash.sha512_crypt


# password derivation functions must produce no more than 8192 bytes of
# output.  ldap_pbkdf2_sha512 provides excellent security and upgradability
# and has so far withstood the test of time, unlike some newer functions.
# DO NOT use MD5 or SHA1-based schemes, or anything older (see passlib
# docs).
password_schemes = ['ldap_pbkdf2_sha512']    # @todo upgrading

password_derivation_function = hash.ldap_pbkdf2_sha512


