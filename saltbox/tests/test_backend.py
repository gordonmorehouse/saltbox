# -*- coding: utf-8 -*-
"""
    Copyright 2012 Gordon Morehouse <gordon@morehouse.me>

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
"""
import unittest

# @todo use nose.

# the various engine test classes should test for the existence of
# the actual external dependencies (sqlite, mysql, memcache, etc)
# in setUp() and skip all tests for the given engine if they aren't
# present, otherwise people would need to install 500 dependencies
# to run tests.  @todo  ....or, test only what's enabled in config.
# ...or, use different test scripts depending on what the user wants
# to test.


class TestGenericBackend(unittest.TestCase):
    """
    Any class inheriting from SaltboxBackend MUST pass these tests.

    Because the Backends can store stuff any way they want, we test them black box
    and DO NOT introspect their data store, ever.
    """

    _sbb_class = None

    @classmethod
    def setUpClass(cls):
        """
        Override this method and replace DictBackend with your SaltboxBackend
        derived class.
        """
        try:
            from saltbox.backend import AlchemyBackend
            cls._sbb_class = AlchemyBackend
        except ImportError:
            pass


    def setUp(self):
        if self._sbb_class is None:
            self.skipTest("backend not instantiated")

        self.sbb = self._sbb_class()


    def test__db_read(self):
        self.sbb._db_write('Owner 1', 'Key 1', 'Password 1')

        # the best we can do here is ensure we always get a consistent string
        # back; the string could change on subsequent writes of the same
        # (owner, key, data) tuple, so we can't e.g. compare 2 reads from 2
        # writes of the same variables.
        str1 = self.sbb._db_read('Owner 1', 'Key 1')
        str2 = self.sbb._db_read('Owner 1', 'Key 1')
        self.assertIsInstance(str1, str)
        self.assertIsInstance(str2, str)
        self.assertFalse(str1 == '')
        self.assertFalse(str2 == '')
        self.assertEqual(str1, str2)

    
    def test__db_read_wrong_owner_wrong_key(self):
        self.sbb._db_write('Owner 1', 'Key 1', 'Password 1')
        
        with self.assertRaises(KeyError):
            self.sbb._db_read('Owner 2', 'Key 2')

    
    def test__db_read_wrong_owner(self):
        self.sbb._db_write('Owner 1', 'Key 1', 'Password 1')
        
        with self.assertRaises(KeyError):
            self.sbb._db_read('Owner 2', 'Key 1')


    def test__db_read_wrong_key(self):
        self.sbb._db_write('Owner 1', 'Key 1', 'Password 1')
        
        with self.assertRaises(KeyError):
            self.sbb._db_read('Owner 1', 'Key 2')


    def test_db_write(self):
        """
        @todo already implicitly done by test_db_read()
        """
        self.sbb._db_write('Owner 1', 'Key 1', 'Password 1')

        # the best we can do here is ensure we get a non-empty string back
        str1 = self.sbb._db_read('Owner 1', 'Key 1')
        self.assertIsInstance(str1, str)
        self.assertFalse(str1 == '')


    def test_db_write_overlong_owner(self):
        foo = ""
        for i in range(0, 515):
            foo = foo + 'A'

        with self.assertRaises(KeyError):
            self.sbb._db_write(foo, 'Key 1', 'Password 1')


    def test_db_write_overlong_key(self):
        foo = ""
        for i in range(0, 515):
            foo = foo + 'A'

        with self.assertRaises(KeyError):
            self.sbb._db_write('Owner 1', foo, 'Password 1')


    def test_db_write_overlong_data(self):
        foo = ""
        for i in range(0, 8195):
            foo = foo + 'A'

        with self.assertRaises(ValueError):
            self.sbb._db_write('Owner 1', 'Key 1', foo)


    def test_db_write_null_owner(self):
        with self.assertRaises(KeyError):
            self.sbb._db_write('', 'Key 1', 'Password 1')

        with self.assertRaises(KeyError):
            self.sbb._db_write(None, 'Key 1', 'Password 1')

    
    def test_db_write_null_key(self):
        with self.assertRaises(KeyError):
            self.sbb._db_write('Owner 1', '', 'Password 1')

        with self.assertRaises(KeyError):
            self.sbb._db_write('Owner 1', None, 'Password 1')

    
    def test_db_write_null_data(self):
        # blank passwords are allowed, so we only check for None, not
        # the empty string in this case
        with self.assertRaises(ValueError):
            self.sbb._db_write('Owner 1', 'Key 1', None)

    
    def test_db_write_owner_key_collision(self):
        self.sbb._db_write('Owner 1', 'Key 1', 'Password 1')

        with self.assertRaises(KeyError):
            # overwrite is illegal, must _db_update
            self.sbb._db_write('Owner 1', 'Key 1', 'Drowssap 1')


    def test_db_update(self):
        self.sbb._db_write('Owner 1', 'Key 1', 'Password 1')

        str1 = self.sbb._db_read('Owner 1', 'Key 1')
        self.assertIsInstance(str1, str)
        self.assertFalse(str1 == '')
        
        self.sbb._db_update('Owner 1', 'Key 1', 'Drowssap 1')
        str1a = self.sbb._db_read('Owner 1', 'Key 1')
        
        self.assertIsInstance(str1a, str)
        self.assertFalse(str1a == '')
        self.assertNotEqual(str1, str1a)


    def test_db_update_overlong_owner(self):
        self.sbb._db_write('Owner 1', 'Key 1', 'Password 1')

        foo = ""
        for i in range(0, 515):
            foo = foo + 'A'

        with self.assertRaises(KeyError):
            self.sbb._db_update(foo, 'Key 1', 'Password 1')


    def test_db_update_overlong_key(self):
        self.sbb._db_write('Owner 1', 'Key 1', 'Password 1')

        foo = ""
        for i in range(0, 515):
            foo = foo + 'A'

        with self.assertRaises(KeyError):
            self.sbb._db_update('Owner 1', foo, 'Password 1')


    def test_db_update_overlong_data(self):
        self.sbb._db_write('Owner 1', 'Key 1', 'Password 1')

        foo = ""
        for i in range(0, 8195):
            foo = foo + 'A'

        with self.assertRaises(ValueError):
            self.sbb._db_update('Owner 1', 'Key 1', foo)


    def test_db_update_wrong_owner_wrong_key(self):
        self.sbb._db_write('Owner 1', 'Key 1', 'Password 1')

        with self.assertRaises(KeyError):
            self.sbb._db_update('Owner 2', 'Key 2', 'Drowssap 1')


    def test_db_update_null_owner(self):
        self.sbb._db_write('Owner 1', 'Key 1', 'Password 1')

        with self.assertRaises(KeyError):
            self.sbb._db_update('', 'Key 1', 'Password 1')

        with self.assertRaises(KeyError):
            self.sbb._db_update(None, 'Key 1', 'Password 1')


    def test_db_update_null_key(self):
        self.sbb._db_write('Owner 1', 'Key 1', 'Password 1')

        with self.assertRaises(KeyError):
            self.sbb._db_update('Owner 1', '', 'Password 1')

        with self.assertRaises(KeyError):
            self.sbb._db_update('Owner 1', None, 'Password 1')


    def test_db_update_null_data(self):
        self.sbb._db_write('Owner 1', 'Key 1', 'Password 1')

        # blank passwords are allowed, so we only check for None, not
        # the empty string in this case
        with self.assertRaises(ValueError):
            self.sbb._db_update('Owner 1', 'Key 1', None)


    def test_db_update_same_data(self):
        # @todo would salt should ensure a change... right?  depends on cryptcontext?
        
        # Since we don't know the original data, an update using the same
        # data (password) must succeed.  Checks against using the same password
        # on update shall be enforced by the application, not this module.
        self.sbb._db_write('Owner 1', 'Key 1', 'Password 1')

        str1 = self.sbb._db_read('Owner 1', 'Key 1')
        self.assertIsInstance(str1, str)
        self.assertFalse(str1 == '')
        
        self.sbb._db_update('Owner 1', 'Key 1', 'Password 1')
        str1a = self.sbb._db_read('Owner 1', 'Key 1')
        
        self.assertIsInstance(str1a, str)
        self.assertFalse(str1a == '')


    def test_db_delete(self):
        self.sbb._db_write('Owner 1', 'Key 1', 'Password 1')
        self.sbb._db_delete('Owner 1', 'Key 1')
        
        with self.assertRaises(KeyError):
            self.sbb._db_read('Owner 1', 'Key 1')


    def test_db_delete_wrong_owner_wrong_key(self):
        self.sbb._db_write('Owner 1', 'Key 1', 'Password 1')

        with self.assertRaises(KeyError):
            self.sbb._db_delete('Owner 2', 'Key 2')


